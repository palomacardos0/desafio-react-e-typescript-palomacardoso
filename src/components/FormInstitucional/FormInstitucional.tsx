import { Formik, Form, Field, ErrorMessage, FormikProps, FormikHelpers } from "formik";
import FormSchema from "./schema/FormSchema";
import style from "./styles.module.css";
import MaskedInput from "react-text-mask";
interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  birth: string;
  phone: string;
  instagram: string;
  check: boolean;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  birth: "",
  phone: "",
  instagram: "",
  check: false,
};

const phoneNumberMask = [
  "(",
  /[0-9]/,

  /\d/,
  ")",
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];
const CPFMask = [
  /[0-9]/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
];
const DateMask = [/[0-9]/, /\d/, ".", /\d/, /\d/, ".", /\d/, /\d/, /\d/, /\d/];

const FormInstitucional = () => {
  const handleFormikSubmit = (values: IFormikValues, actions: FormikHelpers<IFormikValues>) => {
    console.log(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <div className={style["form-content"]}>
      <h2 className={style["form-title"]}>PREENCHA O FORMULÁRIO </h2>
      <Formik
        initialValues={initialValues}
        validationSchema={FormSchema}
        onSubmit={handleFormikSubmit}
      >
        {(props: FormikProps<any>) => {
          const { handleChange } = props;
          return (
            <Form className={style["form__container"]}>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="name">
                  Nome
                </label>
                <Field
                  id="name"
                  name="name"
                  placeholder="Seu nome completo"
                  className={style["form__input"]}
                />
                <ErrorMessage className={style["form__error"]} component="span" name="name" />
              </div>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="email">
                  E-mail
                </label>
                <Field
                  id="email"
                  name="email"
                  placeholder="Seu e-mail"
                  className={style["form__input"]}
                />
                <ErrorMessage className={style["form__error"]} component="span" name="email" />
              </div>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="cpf">
                  CPF
                </label>
                <Field id="cpf" name="cpf">
                  {({ field }: any) => (
                    <MaskedInput
                      {...field}
                      mask={CPFMask}
                      id="cpf"
                      placeholder="000 000 000 00"
                      type="text"
                      onChange={handleChange}
                      className={style["form__input"]}
                    />
                  )}
                </Field>
                <ErrorMessage className={style["form__error"]} component="span" name="cpf" />
              </div>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="birth">
                  Data de Nascimento:
                </label>
                <Field id="birth" name="birth">
                  {({ field }: any) => (
                    <MaskedInput
                      {...field}
                      mask={DateMask}
                      id="birth"
                      placeholder="00 . 00 . 0000"
                      type="text"
                      onChange={handleChange}
                      className={style["form__input"]}
                    />
                  )}
                </Field>
                <ErrorMessage className={style["form__error"]} component="span" name="birth" />
              </div>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="phone">
                  Telefone:
                </label>
                <Field id="phone" name="phone">
                  {({ field }: any) => (
                    <MaskedInput
                      {...field}
                      mask={phoneNumberMask}
                      id="phone"
                      placeholder="(+00) 00000 0000 "
                      type="text"
                      onChange={handleChange}
                      className={style["form__input"]}
                    />
                  )}
                </Field>

                <ErrorMessage className={style["form__error"]} component="span" name="phone" />
              </div>
              <div className={style["form__imput-content"]}>
                <label className={style["form__label"]} htmlFor="instagram">
                  Instagram
                </label>
                <Field
                  id="instagram"
                  name="instagram"
                  placeholder="@seuuser"
                  className={style["form__input"]}
                />
                <ErrorMessage className={style["form__error"]} component="span" name="instagram" />
              </div>
              <div
                className={`${style["form__imput-content"]}
            ${style["terms-conditions"]}`}
              >
                <label className={style["form__label"]} htmlFor="check">
                  <span>*</span>Declaro que li e aceito
                </label>
                <Field
                  id="check"
                  name="check"
                  type="checkbox"
                  className={style["form__input-check"]}
                />
                <ErrorMessage className={style["form__error"]} component="span" name="check" />
              </div>
              <button type="submit" className={style["form__submit"]}>
                CADASTRE-SE
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};
export { FormInstitucional };
