import * as Yup from "yup";
const phoneRegExp = /^\([0-9]{2}\) [0-9]?[0-9]{4}-[0-9]{4}$/;
const CPFRegExp = /[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}-?[0-9]{2}/;
const birthRegExp = /(0[1-9]|[12][0-9]|3[01])[..](0[1-9]|1[012])[..](19|20|)\d\d/;

export default Yup.object().shape({
  name: Yup.string().required("O campo nome é obrigatório").min(5, "Nome inválido"),
  email: Yup.string().required("O campo e-mail é obrigatório").email("Email inválido"),
  cpf: Yup.string().required("O campo CPF é obrigatório").matches(CPFRegExp, "CPF inválido"),
  birth: Yup.string()
    .required("O campo nascimento é obrigatório")
    .matches(birthRegExp, "Data inválida"),
  phone: Yup.string()
    .required("O campo telefone é obrigatório")
    .matches(phoneRegExp, "Número inválido"),
  instagram: Yup.string().required("O campo instagram é obrigatório"),
  check: Yup.bool().oneOf([true], "Campo obrigatório"),
});
