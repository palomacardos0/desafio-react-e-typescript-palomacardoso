import React, { useState } from "react";
import style from "./styles.module.css";

import facebookIcon from "./assets/images/svg/facebook-icon.svg";
import instragramIcon from "./assets/images/svg/instagram-icon.svg";
import youtubeIcon from "./assets/images/svg/youtube-icon.svg";
import linkedinIcon from "./assets/images/svg/linkedin-icon.svg";
import twitterIcon from "./assets/images/svg/twitter-icon.svg";

const FooterLinks = () => {
  const [institutional, setInstitutional] = useState(false);
  const [duvidas, setDuvidas] = useState(false);
  const [faleConosco, setFaleConosco] = useState(false);

  function handleToggleAccodion(event: React.MouseEvent<HTMLHeadingElement, MouseEvent> | any) {
    const id = event.target.id;
    if (id === "institucional") {
      setInstitutional(!institutional);
    } else if (id === "duvidas") {
      setDuvidas(!duvidas);
    } else if (id === "fale-conosco") {
      setFaleConosco(!faleConosco);
    }
  }
  return (
    <section className={style["links-container"]}>
      <div
        className={`${style["links-container__institucional"]} ${
          institutional ? style["oppened"] : ""
        }`}
      >
        <h4
          className={style["links-container__title"]}
          id="institucional"
          onClick={handleToggleAccodion}
        >
          Institucional
        </h4>
        <ul className={style["accordion-item"]}>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="quem-somos">
              Quem Somos
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="politica-de-privacidade">
              Política de Privacidade
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="segurança">
              Segurança
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="seja-um-revendedor">
              Seja um Revendedor
            </a>
          </li>
        </ul>
      </div>
      <div className={`${style["links-container__duvidas"]} ${duvidas ? style["oppened"] : ""}`}>
        <h4 className={style["links-container__title"]} id="duvidas" onClick={handleToggleAccodion}>
          Dúvidas
        </h4>
        <ul className={style["accordion-item"]}>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="entrega">
              Entrega
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="pagamento">
              Pagamento
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="trocas-e-devoluções">
              Trocas e Devoluções
            </a>
          </li>
          <li className={style["links-container__item"]}>
            <a className={style["links-container__link"]} href="dúvidas-frequentes">
              Dúvidas Frequentes
            </a>
          </li>
        </ul>
      </div>
      <div
        className={`${style["links-container__fale-conosco"]} ${
          faleConosco ? style["oppened"] : ""
        }`}
      >
        <h4
          className={style["links-container__title"]}
          id="fale-conosco"
          onClick={handleToggleAccodion}
        >
          Fale Conosco
        </h4>
        <ul className={style["accordion-item"]}>
          <li className={style["links-container__item"]}>
            <strong>Atendimento ao Consumidor</strong>
          </li>
          <li className={style["links-container__item"]}>(11) 4159 9504</li>
          <li className={style["links-container__item"]}>
            <strong>Atendimento Online</strong>
          </li>
          <li className={style["links-container__item"]}>(11) 99433-8825</li>
        </ul>
      </div>
      <div className={style["links-container__social"]}>
        <ul className={style["links-container__social-links"]}>
          <li className={style["links-container__social-item"]}>
            <a href="https://www.facebook.com/digitalm3" target="_blank" rel="noreferrer">
              <img src={facebookIcon} alt="Facebook" />
            </a>
          </li>
          <li className={style["links-container__social-item"]}>
            <a href=" https://www.instagram.com/m3.ecommerce/" target="_blank" rel="noreferrer">
              <img src={instragramIcon} alt="Intagram" />
            </a>
          </li>
          <li className={style["links-container__social-item"]}>
            <a href="https://twitter.com/" target="_blank" rel="noreferrer">
              <img src={twitterIcon} alt="Twitter" />
            </a>
          </li>
          <li className={style["links-container__social-item"]}>
            <a
              href="https://www.youtube.com/channel/UCW4o86gZG_ceA8CmHltXeXA"
              target="_blank"
              rel="noreferrer"
            >
              <img src={youtubeIcon} alt="Youtube" />
            </a>
          </li>
          <li className={style["links-container__social-item"]}>
            <a
              href="https://www.linkedin.com/company/m3ecommerce/"
              target="_blank"
              rel="noreferrer"
            >
              <img src={linkedinIcon} alt="Linkedin" />
            </a>
          </li>
        </ul>
        <a
          className={style["links-container__link-site"]}
          href="www.loremipsum.com"
          target="_blank"
          rel="noreferrer"
        >
          www.loremipsum.com
        </a>
      </div>
    </section>
  );
};

export { FooterLinks };
