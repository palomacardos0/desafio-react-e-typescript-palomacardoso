import * as Yup from "yup";

export default Yup.object().shape({
  email: Yup.string().required("Digite seu e-mail").email("Email inválido"),
});
