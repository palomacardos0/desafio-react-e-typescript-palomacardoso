import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import FormSchema from "./schema/FormSchema";
import style from "./styles.module.css";

interface IFormikValue {
  email: string;
}

const initialValues = {
  email: "",
};

const FooterNewsletter = () => {
  const handleFormikSubmit = (value: IFormikValue, actions: FormikHelpers<IFormikValue>) => {
    console.log(value);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <section className={style["newsletter"]}>
      <Formik
        onSubmit={handleFormikSubmit}
        initialValues={initialValues}
        validationSchema={FormSchema}
      >
        <Form className={style["newsletter__form"]}>
          <h3 className={style["newsletter__title"]}>assine nossa newsletter</h3>
          <div className={style["newsletter__container-input"]}>
            <Field name="email" placeholder="E-mail" className={style["newsletter__input"]} />
            <ErrorMessage component="span" name="email" className={style["form__error"]} />
            <button type="submit" className={style["newsletter__submit"]}>
              ENVIAR
            </button>
          </div>
        </Form>
      </Formik>
    </section>
  );
};

export { FooterNewsletter };
