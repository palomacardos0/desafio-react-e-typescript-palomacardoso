import style from "./styles.module.css";
import closeIcon from "./assets/images/svg/close-icon.svg";
interface IMenuProps {
  openMenu: boolean;
  handleOpenMenu: () => void;
}
const Menu = ({ handleOpenMenu, openMenu }: IMenuProps) => {
  return (
    <>
      <div className={`${style["header-bottom"]} ${openMenu && style["menuOpen"]}`}>
        {openMenu && (
          <>
            <a href="/login" className={style["header-bottom__login"]}>
              ENTRAR
            </a>
            <button className={style["header-bottom__closeIcon"]} onClick={handleOpenMenu}>
              <img src={closeIcon} alt="Fechar menu" />
            </button>
          </>
        )}
        <ul className={style["header-bottom__menu-principal"]}>
          <li className={style["header-bottom__menu-principal-li"]}>
            <a
              className={style["header-bottom__menu-principal-item"]}
              href="/cursos"
              title="CURSOS"
            >
              CURSOS
            </a>
          </li>

          <li className={style["header-bottom__menu-principal-li"]}>
            <a
              className={style["header-bottom__menu-principal-item"]}
              href="/saiba-mais"
              title="SAIBA MAIS"
            >
              SAIBA MAIS
            </a>
          </li>
        </ul>
      </div>
      <div
        className={`${style["header-bottom__menu"]} ${openMenu && style["overflowOpen"]}`}
        onClick={handleOpenMenu}
      ></div>
    </>
  );
};

export { Menu };
