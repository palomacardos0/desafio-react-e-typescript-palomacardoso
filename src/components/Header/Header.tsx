import style from "./style.module.css";

import logoM3 from "./assets/images/png/m3-academy-logo.png";
import minicartIcon from "./assets/images/svg/cart-icon.svg";
import MenuIcon from "./assets/images/svg/hamburguer-menu-icon.svg";
import searchIcon from "./assets/images/svg/search-icon.svg";
import { useState } from "react";
import { Menu } from "../Menu/Menu";

const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);

  function handleOpenMenu() {
    setOpenMenu(!openMenu);
  }
  return (
    <header className={style["header"]}>
      <div className={style["header-container"]}>
        <div className={style["header-top"]}>
          <button className={style["header-top__menu-button"]} onClick={handleOpenMenu}>
            <img src={MenuIcon} alt="Menu Ícone" />
          </button>
          <a href="/" className={style["header-top__logo"]}>
            <img src={logoM3} alt="Logo M3 Academy" />
          </a>
          <div className={style["header-top__searchBar"]}>
            <input
              type="text"
              className={style["header-top__search-input"]}
              placeholder="Buscar..."
            />
            <button className={style["header-top__search-button"]}>
              <img src={searchIcon} alt="Buscar" />
            </button>
          </div>
          <div className={style["header-top__account"]}>
            <a href="/login">ENTRAR</a>
            <button>
              <img src={minicartIcon} alt="Carrinho" />
            </button>
          </div>
        </div>

        <Menu handleOpenMenu={handleOpenMenu} openMenu={openMenu} />
      </div>
    </header>
  );
};

export { Header };
