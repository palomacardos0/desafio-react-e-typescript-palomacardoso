import React from "react";
import style from "./styles.module.css";

interface selectedMenuOptionProps {
  id: "about" | "payment" | "delivery" | "security" | "devolution" | "contact";
  title: string;
}

interface MenuProps {
  sections?: Array<selectedMenuOptionProps>;
  handleClickButton: (id: string) => void;
  selectedMenuOptionId: string;
}

const NavigationBarInstitucional = ({
  handleClickButton,
  sections,
  selectedMenuOptionId,
}: MenuProps) => {
  return (
    <section className={style["menu-institucional__content"]}>
      <ul>
        {sections?.map((section) => (
          <li
            key={section.id}
            onClick={() => {
              handleClickButton(section.id);
            }}
            className={`${style["menu-institucional__section"]} ${
              selectedMenuOptionId === section.id ? style["active"] : ""
            }`}
          >
            <button>{section.title}</button>
          </li>
        ))}
      </ul>
    </section>
  );
};

export { NavigationBarInstitucional };
