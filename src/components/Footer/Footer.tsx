import { FooterLinks } from "../FooterLinks/FooterLinks";
import { FooterNewsletter } from "../FooterNewsletter/FooterNewsletter";

import style from "./styles.module.css";

import mastercardIcon from "./assets/images/png/mastercard-icon.png";
import visaIcon from "./assets/images/png/visa-icon.png";
import americanExpressIcon from "./assets/images/png/american-express-icon.png";
import eloIcon from "./assets/images/png/elo-icon.png";
import hipercardIcon from "./assets/images/png/hipercard-icon.png";
import paypalIcon from "./assets/images/png/paypal-icon.png";
import boletoIcon from "./assets/images/png/boleto-icon.png";
import vtexPciIcon from "./assets/images/png/vtex-pci-icon.png";
import vtexIcon from "./assets/images/svg/vtex-icon.svg";
import m3Icon from "./assets/images/svg/m3-icon.svg";
import whatsappIcon from "./assets/images/svg/whatsapp-icon.svg";
import upIcon from "./assets/images/svg/up-icon.svg";

const Footer = () => {
  function buttonTop() {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }
  return (
    <footer>
      <a
        href="https://api.whatsapp.com/send?phone=55119999999999"
        target="_blank"
        rel="noreferrer"
        className={style["whatsapp-button"]}
      >
        <img src={whatsappIcon} alt="Whatsapp" />
      </a>
      <button onClick={buttonTop} className={style["up-button"]}>
        <img src={upIcon} alt="Topo da página" />
      </button>
      <FooterNewsletter />
      <FooterLinks />
      <section className={style["footer-infos"]}>
        <div className={style["footer-infos__container"]}>
          <p className={`${style["footer-infos__text"]} ${style["text-desktop"]}`}>
            Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing <br />
            Elit, Sed Do Eiusmod Tempor
          </p>

          <div className={style["footer-infos__list"]}>
            <ul className={style["footer-infos__payment"]}>
              <li>
                <img src={mastercardIcon} alt="Mastercard" />
              </li>
              <li>
                <img src={visaIcon} alt="Visa" />
              </li>
              <li>
                <img src={americanExpressIcon} alt="American Express" />
              </li>
              <li>
                <img src={eloIcon} alt="Elo" />
              </li>
              <li>
                <img src={hipercardIcon} alt="Hipercard" />
              </li>
              <li>
                <img src={paypalIcon} alt="Paypal" />
              </li>
              <li>
                <img src={boletoIcon} alt="Boleto" />
              </li>
            </ul>
            <div className={style["footer-infos__divider"]}></div>
            <ul className={style["footer-infos__security"]}>
              <li>
                <img src={vtexPciIcon} alt="Vtex PCI" />
              </li>
            </ul>
          </div>
          <p className={`${style["footer-infos__text"]} ${style["text-mobile"]}`}>
            Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit,
          </p>
          <div className={style["footer-infos__development"]}>
            <p className={style["footer-infos__powered-by"]}>Powered by</p>
            <img src={vtexIcon} alt="Vtex" />
            <p className={style["footer-infos__developed-by"]}>Developed by</p>
            <img src={m3Icon} alt="M3" />
          </div>
        </div>
      </section>
    </footer>
  );
};

export { Footer };
