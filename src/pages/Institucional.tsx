import React, { useState } from "react";
import { Header } from "../components/Header/Header";
import { Footer } from "../components/Footer/Footer";
import { NavigationBarInstitucional } from "../components/NavigationBarInstitucional/NavigationBarInstitucional";
import style from "./styles.module.css";
import { ContentInstitucional } from "../components/ContentInstitucional/ContentInstitucional";

import homeIcon from "./assets/images/svg/home-icon.svg";
import arrowIcon from "./assets/images/svg/arrow-right-icon.svg";

interface selectedMenuOptionProps {
  id: "about" | "payment" | "delivery" | "security" | "devolution" | "contact";
  title: string;
}

const section: selectedMenuOptionProps[] = [
  {
    id: "about",
    title: "Sobre",
  },
  {
    id: "payment",
    title: "Forma de Pagamento",
  },
  {
    id: "delivery",
    title: "Entrega",
  },
  {
    id: "devolution",
    title: "Troca e Devolução",
  },
  {
    id: "security",
    title: "Segurança e Privacidade",
  },
  {
    id: "contact",
    title: "Contato",
  },
];
const Institucional = () => {
  const [selectedMenuOptionId, setSelectedMenuOptionId] = useState("about");

  function handleClickButton(id: string) {
    setSelectedMenuOptionId(id);
  }
  return (
    <>
      <Header />
      <section className={style["container-institucional"]}>
        <div className={style["container-institucional__breadcrumb"]}>
          <a href="/">
            <img src={homeIcon} alt="Icone Home" />
          </a>
          <img src={arrowIcon} alt="Seta para direita" />
          <a href="/institucional" className={style["container-institucional__breadcrumb-link"]}>
            INSTITUCIONAL
          </a>
        </div>
        <h1 className={style["container-institucional__title"]}>INSTITUCIONAL</h1>
        <section className={style["container-institucional__content"]}>
          <NavigationBarInstitucional
            sections={section}
            handleClickButton={handleClickButton}
            selectedMenuOptionId={selectedMenuOptionId}
          />
          <ContentInstitucional selectedMenuOptionId={selectedMenuOptionId} />
        </section>
      </section>
      <Footer />
    </>
  );
};

export { Institucional };
